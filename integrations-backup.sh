#!/bin/bash

# Collect information

# Current date
DATE="$(date +"%Y-%m-%d-%H-%M-%S")"

# Backup directory
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
backup_dir="${1:-${SCRIPT_DIR}/backup}"
current_backup_dir="${backup_dir}/mailman-${DATE}"


# Backup

# Create backup directories

echo "Creating backup directory ${current_backup_dir}"

mkdir -p "${current_backup_dir}"

## Mailman Core

echo "Backup Mailman Core - archiving"

sudo tar -C ${SCRIPT_DIR}/data --exclude='mailman/core/var/logs' --use-compress-program="gzip --rsyncable" -cf "${current_backup_dir}/core_backup.tar.gz" mailman/core

## Mailman Web

echo "Backup Mailman Web - archiving"

sudo tar -C ${SCRIPT_DIR}/data --exclude='mailman/web/logs'      --use-compress-program="gzip --rsyncable" -cf "${current_backup_dir}/web_backup.tar.gz" mailman/web 

## Mailman Database

db_backup_file="db_dump.sql"

echo "Backup Mailman Mailman Database - dump databases"

docker-compose exec mailman-database pg_dumpall -c -U mailman > "${current_backup_dir}/${db_backup_file}"

echo "Backup Mailman Mailman Database - archiving db dump"

sudo tar -C ${current_backup_dir} --use-compress-program="gzip --rsyncable" --remove-files -cf "${current_backup_dir}/db_backup.tar.gz" "${db_backup_file}"





