# mailcow-integrations

Integrate mailcow with
- docker-mailman: based on https://gitbucket.pgollor.de/docker/mailman-mailcow-integration/
- collabora CORE
- Nextcloud
- Gitlab
- 

# Install

1) Setup mailcow
2) Install nextcloud running `./helper-scripts/nextcoud.sh -i` from your mailcow-dockerized directory
3) Copy content (except the .git directory) into mailcow folder (usually /opt/mailcow-dockerized)
4) Run `integrations-install` located in the mailcow-dockerized directory
   - Add mailman smtp and admin user mailboxes in mailcow
   - Setup list mail domains in mailcow
     - Add spf, dmarc, dkim and MX DNS entries

# TODO

- Automatically add additional san entries to mailcow.conf
- Add nextcloud background cronjob automatically on host machine
  `*/5  *  *  *  * docker exec -i -u www-data $(docker ps -f name=php-fpm-mailcow -q) bash -c "php /web/nextcloud/cron.php"`
- Automatically enable BOSH on port 5281
- Use https://hub.docker.com/r/pschlieker/vdirsyncer-docker for sogo/nextcloud dav sync
- Nextcloud - PicoCMS
  Add inside server declaration in nextcloud nginx config
  ```
  location ^~ /sites/ {
    proxy_set_header X-Forwarded-Host $host:$server_port;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Server $host;
    proxy_pass https://cloud.pipe.parts/index.php/apps/cms_pico/pico_proxy/;
    proxy_ssl_server_name on;
  }
  ```
