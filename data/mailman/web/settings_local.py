# locale
LANGUAGE_CODE = 'de-de'

# disable social authentication
DJANGO_SOCIAL_AUTH_PROVERS = []

DEFAULT_FROM_EMAIL='mailman@example.com'

DEBUG = False
# server ip 
import socket
MAILMAN_ARCHIVER_FROM = (socket.gethostbyname("mailman-core"),)
